package com.intemdox.intemdoxplayer.services;


import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.intemdox.intemdoxplayer.SongManager;
import com.intemdox.intemdoxplayer.Utils;
import com.intemdox.intemdoxplayer.events.PlayPauseEvent;
import com.intemdox.intemdoxplayer.events.RewindSong;
import com.intemdox.intemdoxplayer.events.SeekbarUpdateEvent;
import com.intemdox.intemdoxplayer.events.SetSeekBarEvent;
import com.intemdox.intemdoxplayer.events.TimeDurationEvent;
import com.intemdox.intemdoxplayer.events.UpdateItemViewEvent;
import com.intemdox.intemdoxplayer.model.Song;
//import com.intemdox.intemdoxplayer.notifications.PlayerNotification;

import java.io.IOException;

public class PlayerService extends Service {

    public static final String TAG = "PlayerService";

    private final Handler handler = new Handler();

    private static PlayerService instance;

    private MediaPlayer mediaPlayer = null;


    public static PlayerService getInstance() {
        return instance;
    }





    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        Toast.makeText(this, "Service was created", Toast.LENGTH_SHORT).show();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (instance == null) instance = this;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
        super.onDestroy();
    }


    private void initMediaPlayer(String path) {
        mediaPlayer = new MediaPlayer();
        Uri uri = Uri.parse(path);
        try {
            if (mediaPlayer != null) {
                if (uri != null) {
                    mediaPlayer.setDataSource(this, uri);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setLooping(false);
                    mediaPlayer.prepare();
                    SongManager.getInstance().getBus().post(new SetSeekBarEvent(mediaPlayer.getDuration()));
                } else {
                    Log.d(TAG, "initMediaPlayer: uri is null");
                }
            } else {
                Log.d(TAG, "initMediaPlayer: MediaPlayer is null");
            }
        } catch (IOException e) {
            Log.e(TAG, "initMediaPlayer: Wrong uri: " + uri, e);
        } catch (Exception e) {
            Log.e(TAG, "initMediaPlayer: ", e);
        }

    }


    /**
     * This method starts song.
     * @param song
     */
    public void play(Song song) {
        if (mediaPlayer == null) {
            initMediaPlayer(song.getPath());
            mediaPlayer.start();
        } else {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            } else {
                mediaPlayer.start();
            }
        }
        SongManager.getInstance().getBus().post(new PlayPauseEvent(mediaPlayer.isPlaying()));
        startPlayProgressUpdater();
    }


    /**
     * This method starts new song.
     * @param song
     */
    public void playNewSong(Song song) {
        stop();
        SongManager.markSongAsPlaying(song);

//        PlayerNotification pn = new PlayerNotification(this);
//        pn.setNotificationTitle(song.getArtist());
//        pn.setNotificationText(song.getTitle());
//        pn.showNotification();

        play(song);
        SongManager.getInstance().getBus().post(new UpdateItemViewEvent());
    }


    /**
     * This method stops song.
     */
    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }


    /**
     * This method changes
     * @param i
     */
    public void mediaPlayerSeekTo(int i) {
        if (mediaPlayer != null) {
            mediaPlayer.seekTo(i);
        }
    }

    public void startPlayProgressUpdater() {
        if (mediaPlayer.isPlaying()) {
            String time = Utils.getTime(mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition()) + "   \\   " + Utils.getTime(mediaPlayer.getDuration());
            SongManager.getInstance().getBus().post(new TimeDurationEvent(time));
            SongManager.getInstance().getBus().post(new SeekbarUpdateEvent(mediaPlayer.getCurrentPosition()));
            Runnable notification = new Runnable() {
                public void run() {
                    startPlayProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        } else {
            mediaPlayer.pause();

        }
    }


    public void on(RewindSong event) {
        int position = event.getPosition();
        mediaPlayer.seekTo(position);
    }

    public int getDuration() {
        if (mediaPlayer != null) {
            return mediaPlayer.getDuration();
        } else {
            //TODO correct -1
            return -1;
        }
    }

}