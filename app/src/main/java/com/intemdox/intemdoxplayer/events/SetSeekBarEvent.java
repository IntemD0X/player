package com.intemdox.intemdoxplayer.events;


public class SetSeekBarEvent {
    private int duration;

    public SetSeekBarEvent(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }
}
