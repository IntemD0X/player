package com.intemdox.intemdoxplayer.events;


public class InsertItemViewEvent {
    private int position;

    public InsertItemViewEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
