package com.intemdox.intemdoxplayer.events;

public class TimeDurationEvent {
    private String time = "";

    public TimeDurationEvent(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }
}
