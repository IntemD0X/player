package com.intemdox.intemdoxplayer.events;


public class SeekbarUpdateEvent {
    private int position;

    public SeekbarUpdateEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
