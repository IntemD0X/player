package com.intemdox.intemdoxplayer.events;


public class PlayPauseEvent {
    private boolean isPlaying;

    public PlayPauseEvent(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public boolean isPlaying() {
        return isPlaying;
    }
}
