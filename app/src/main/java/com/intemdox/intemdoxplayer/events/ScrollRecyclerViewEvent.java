package com.intemdox.intemdoxplayer.events;


public class ScrollRecyclerViewEvent {
    private int index;

    public ScrollRecyclerViewEvent(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
