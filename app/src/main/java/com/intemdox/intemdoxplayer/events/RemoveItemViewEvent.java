package com.intemdox.intemdoxplayer.events;


public class RemoveItemViewEvent {
    private int position;

    public RemoveItemViewEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
