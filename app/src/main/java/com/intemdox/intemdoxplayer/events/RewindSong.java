package com.intemdox.intemdoxplayer.events;


public class RewindSong {
    private int position;

    public RewindSong(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
