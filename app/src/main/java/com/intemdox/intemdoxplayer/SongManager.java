package com.intemdox.intemdoxplayer;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intemdox.intemdoxplayer.events.UpdateItemViewEvent;
import com.intemdox.intemdoxplayer.exceptions.SongListException;
import com.intemdox.intemdoxplayer.fragments.PlaylistFragment;
import com.intemdox.intemdoxplayer.model.Song;
import com.squareup.otto.Bus;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SongManager {
    public static final String TAG = "SongManager";

    public static int currentSongPosition = 0;

    private static SongManager INSTANCE;
    private static List<Song> songs;
    private static List<Song> likedList;

    private static int songsAmount = 0;

    private Bus bus;


    private SongManager() {
        this.bus = new Bus();
    }


    public static void initialize() {
        if (INSTANCE == null) {
            INSTANCE = new SongManager();
        }
    }


    public static SongManager getInstance() {
        return INSTANCE;
    }

    public Bus getBus() {
        return bus;
    }

    /**
     * Initializes the main playlist
     *
     * @param context - context.
     */
    public void initializeBasicPlaylist(Context context) {
        if (songs != null) {
            songs.clear();
        }
        songs = getSongList(context);
    }

    /**
     * Initializes a playlist of your favorite songs
     */
    public void initializeLikedlist() {
        if (likedList != null) {
            likedList.clear();
        }
        likedList = getLikedlist();
    }


    /**
     * @param context context.
     * @return the main playlist.
     */
    public List<Song> getPlaylist(Context context) {
        if (songs == null) {
            initializeBasicPlaylist(context);
        }
        return songs;
    }


    /**
     * Return a list of songs, depending on the parameter. May return list of all songs or likedlist
     *
     * @param context  - context.
     * @param playlist - constant from PlaylistFragment class.
     * @return song list
     * @throws SongListException
     */
    public List<Song> getPlaylist(Context context, int playlist) throws SongListException {
        switch (playlist) {
            case PlaylistFragment.FULL_LIST:
                songs = getPlaylist(context);
                return songs;
            case PlaylistFragment.LIKED_LIST:
                if (likedList == null) {
                    initializeLikedlist();
                }
                return likedList;
            default:
                throw new SongListException();
        }
    }


    /**
     * This method add song in Liked list. Song marked sa "liked".
     *
     * @param song - song, which will be added in likedlist.
     */
    public void addSongToLikedlist(Song song) {
        song.markAsLike(true);
        likedList.add(song);
        writeInLikedlist();
    }

    /**
     * This method remove a song from likedlist.
     *
     * @param song - song, which will be removed from likedlist.
     */
    public void deleteSongFromLikedlist(Song song) {
        likedList.remove(song);
        song.markAsLike(false);
        writeInLikedlist();
    }

    /**
     * This method gets a song from playlist.
     *
     * @param context  - context.
     * @param position - position in List<>.
     * @param playlist - main(0) or liked(1) playlist.
     * @return Song
     * @throws SongListException - throw when song not found.
     */
    public Song getSong(Context context, int position, int playlist) throws SongListException {
        return getPlaylist(context, playlist).get(position);
    }


    /**
     * This method read songs from file. Used gson serialization.
     *
     * @return List of songs.
     */
    private static List<Song> getLikedlist() {
        List<Song> result;
        String json = "";
        StringBuilder sb = new StringBuilder();
        File path = Environment.getExternalStorageDirectory();
        File file = new File(path, "likedlist.playlist");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            br.close();
            json = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        result = new Gson().fromJson(json, new TypeToken<List<Song>>() {
        }.getType());
        if (result == null) {
            result = new ArrayList<>();
        }
        return result;
    }


    /**
     * Write all songs which marked as "liked" in likedList
     */
    private void writeInLikedlist() {
        File path = Environment.getExternalStorageDirectory();
        File file = new File(path, "likedlist.playlist");

        String json = new Gson().toJson(likedList, new TypeToken<List<Song>>() {
        }.getType());
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(json);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SongManager.getInstance().getBus().post(new UpdateItemViewEvent());
    }


    /**
     * The method find all audio files in external storage.
     *
     * @param context - context.
     * @return list of all songs in external storage.
     */
    private static List<Song> getSongList(Context context) {
        List<Song> result = new ArrayList<>();
        ContentResolver musicResolver = context.getContentResolver();
        Uri externalContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] STAR = {"*"};
        Cursor cursor = musicResolver.query(externalContentUri, STAR, null, null, null);
        a(cursor, result);

        return result;
    }

    /**
     * This method get params for all song from result list, then creates new Song with this params
     * and add it in songlist,
     *
     * @param cursor - Object of Cursor class.
     * @param result - A list of which will be added songs.
     */
    private static void a(Cursor cursor, List<Song> result) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {

                //get columns
                int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
                int pathColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
                //add songList
                do {
                    String thisTitle = cursor.getString(titleColumn);
                    String thisArtist = cursor.getString(artistColumn);
                    String thisPath = cursor.getString(pathColumn);

                    Song res = new Song();
                    res.setArtist(thisArtist);
                    res.setTitle(thisTitle);
                    res.setPath(thisPath);
                    res.setId(songsAmount);
                    result.add(res);
                    songsAmount++;
                } while (cursor.moveToNext());

            } else {
                Log.d(TAG, "getSongList: проблама с cursor.moveToFirst");
            }
        } else {
            Log.d(TAG, "getSongList: НЕ РАБОТАЕТ ПОИСК ПО ДАННОЙ ПЕРЕМЕННОЙ");
        }

    }


    /**
     * This method mark song in likedlist ans songlist as Playing.
     *
     * @param song - song, which need mark as playing.
     */
    public static void markSongAsPlaying(Song song) {
        for (Song a : songs) {
            if (a.equals(song)) {
                a.markAsPlaying(true);
            } else {
                a.markAsPlaying(false);
            }
        }
        for (Song a : likedList) {
            if (a.equals(song)) {
                a.markAsPlaying(true);
            } else {
                a.markAsPlaying(false);
            }
        }
    }
}
