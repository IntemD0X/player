package com.intemdox.intemdoxplayer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.intemdox.intemdoxplayer.MainActivity;
import com.intemdox.intemdoxplayer.R;
import com.intemdox.intemdoxplayer.SongManager;
import com.intemdox.intemdoxplayer.events.InsertItemViewEvent;
import com.intemdox.intemdoxplayer.events.RemoveItemViewEvent;
import com.intemdox.intemdoxplayer.exceptions.SongListException;
import com.intemdox.intemdoxplayer.fragments.PlaylistFragment;
import com.intemdox.intemdoxplayer.model.Song;
import com.intemdox.intemdoxplayer.services.PlayerService;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.SongViewHolder> {

    public static final String TAG = "PlayListAdapter";

    private MainActivity main;

    private int playlist;


    public PlaylistAdapter(Context context, int playlist) {
        main = (MainActivity) context;
        this.playlist = playlist;
        SongManager.getInstance().getBus().unregister(this);
    }


    @Override
    public PlaylistAdapter.SongViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.model_song, parent, false);
        TextView tvArtist = (TextView) v.findViewById(R.id.song_artist);
        TextView tvTitle = (TextView) v.findViewById(R.id.song_title);
        ImageButton btnLike = (ImageButton) v.findViewById(R.id.btnLike);
        return new SongViewHolder(v, tvArtist, tvTitle, btnLike);
    }


    @Override
    public void onBindViewHolder(final SongViewHolder songViewHolder, final int position) {
        try {
            Song song = SongManager.getInstance().getSong(main, position, playlist);
            if (SongManager.getInstance().getPlaylist(main, PlaylistFragment.LIKED_LIST)
                    .contains(song)) {
                songViewHolder.imageButton.setImageResource(R.drawable.ic_star_black_24dp);
                song.markAsLike(true);
            }
            if (song.isLike()) {
                songViewHolder.imageButton.setImageResource(R.drawable.ic_star_black_24dp);
            } else {
                songViewHolder.imageButton.setImageResource(R.drawable.ic_star_border_black_24dp);
            }
            songViewHolder.tvTitle.setText(song.getTitle());
            songViewHolder.tvArtist.setText(song.getArtist());
            songViewHolder.itemView.setEnabled(true);
            songViewHolder.itemView.setVisibility(View.VISIBLE);
            songViewHolder.imageButton.setEnabled(true);
            songViewHolder.imageButton.setVisibility(View.VISIBLE);
            if (song.isPlaying()) {
                songViewHolder.itemView.setBackgroundColor(main.getResources()
                        .getColor(R.color.colorPrimaryLight));
                songViewHolder.itemView.setAlpha(1.0f);
                songViewHolder.tvArtist.setTextColor(main.getResources()
                        .getColor(R.color.primary_text));
                songViewHolder.tvTitle.setTextColor(main.getResources()
                        .getColor(R.color.primary_text));
            } else {
                songViewHolder.itemView.setBackgroundColor(main.getResources()
                        .getColor(R.color.white));
                songViewHolder.tvArtist.setTextColor(main.getResources()
                        .getColor(R.color.secondary_text));
                songViewHolder.tvTitle.setTextColor(main.getResources()
                        .getColor(R.color.secondary_text));
            }
        } catch (SongListException e) {
            songViewHolder.itemView.setEnabled(false);
            songViewHolder.imageButton.setEnabled(false);
            Log.e(TAG, "onBindViewHolder: Song not found.", e);
        }

        songViewHolder.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!SongManager.getInstance().getSong(main, position, playlist).isLike()) {
                        SongManager.getInstance().addSongToLikedlist(SongManager.getInstance()
                                .getSong(main, position, playlist));
                        SongManager.getInstance().getBus().post(new InsertItemViewEvent(position));
                        songViewHolder.imageButton.setImageResource(R.drawable.ic_heart_full);
                    } else {
                        SongManager.getInstance().deleteSongFromLikedlist(SongManager.getInstance()
                                .getSong(main, position, playlist));
                        SongManager.getInstance().getBus().post(new RemoveItemViewEvent(position));
                        //SongManager.getInstance().getBus().post(new UpdateItemViewEvent());
                        songViewHolder.imageButton.setImageResource(R.drawable.ic_heart_outline);
                    }
                } catch (SongListException e) {
                    Log.e(TAG, "onClick: Song not found", e);
                }
            }
        });

        songViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SongManager.currentSongPosition = position;
                try {
                    PlayerService.getInstance().playNewSong(SongManager.getInstance()
                            .getSong(main, position, playlist));
                } catch (SongListException e) {
                    Log.e(TAG, "onClick: Song not found", e);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        try {
            return SongManager.getInstance().getPlaylist(main, playlist).size();
        } catch (SongListException e) {
            Log.d(TAG, "getItemCount: SongListException. Return 0;");
            return 0;
        }
    }


    class SongViewHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener {
        TextView tvArtist;
        TextView tvTitle;
        ImageButton imageButton;


        public SongViewHolder(View itemView, TextView tvArtist, TextView tvTitle,
                              ImageButton imageButton) {
            super(itemView);
            this.tvArtist = tvArtist;
            this.tvTitle = tvTitle;
            this.imageButton = imageButton;
        }


        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select the Action");
            menu.add(0, v.getId(), 0, "info");
            menu.add(0, v.getId(), 0, "delete");
        }
    }
}
