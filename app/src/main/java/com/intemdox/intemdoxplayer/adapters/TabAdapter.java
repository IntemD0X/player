package com.intemdox.intemdoxplayer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.intemdox.intemdoxplayer.fragments.PlaylistFragment;

public class TabAdapter extends FragmentPagerAdapter {
    //public static final String TAG = "TabAdapter";
    private int numberOfTabs;
    private PlaylistFragment first;
    private PlaylistFragment second;

    public TabAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                if (first == null) {
                    first = PlaylistFragment.newInstance(PlaylistFragment.LIST_TYPE,
                            PlaylistFragment.FULL_LIST);
                }
                return first;
            case 1:
                if (second == null) {
                    second = PlaylistFragment.newInstance(PlaylistFragment.LIST_TYPE,
                            PlaylistFragment.LIKED_LIST);
                }
                return second;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "PlayList";
            case 1:
                return "LikedList";
            default:
                return "";
        }
    }
}
