package com.intemdox.intemdoxplayer.model;


public class Song {
    //   public static final String TAG = "Song";
    private String title;
    private String artist;
    private String path;
    private int id;
    private boolean isLike;
    private boolean isPlaying;

    public Song() {
    }


    public boolean isLike() {
        return isLike;
    }


    public void markAsLike(boolean isLike) {
        this.isLike = isLike;
    }


    public String getTitle() {
        if (title != null) {
            return title;
        } else {
            return "NULL POINTER EXCEPTION";
        }
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public void setArtist(String artist) {
        this.artist = artist;
    }


    public void setPath(String path) {
        this.path = path;
    }


    public String getArtist() {
        return artist;
    }


    public String getPath() {
        return path;
    }


    public boolean isPlaying() {
        return isPlaying;
    }


    public void markAsPlaying(boolean a) {
        isPlaying = a;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Song song = (Song) o;

        return path.equals(song.path);

    }


    @Override
    public int hashCode() {
        return path.hashCode();
    }
}