package com.intemdox.intemdoxplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.intemdox.intemdoxplayer.adapters.TabAdapter;
import com.intemdox.intemdoxplayer.events.PlayPauseEvent;
import com.intemdox.intemdoxplayer.events.ScrollRecyclerViewEvent;
import com.intemdox.intemdoxplayer.events.SeekbarUpdateEvent;
import com.intemdox.intemdoxplayer.events.SetSeekBarEvent;
import com.intemdox.intemdoxplayer.events.TimeDurationEvent;
import com.intemdox.intemdoxplayer.exceptions.SongListException;
import com.intemdox.intemdoxplayer.fragments.PlaylistFragment;
import com.intemdox.intemdoxplayer.model.Song;
import com.intemdox.intemdoxplayer.services.PlayerService;
import com.squareup.otto.Subscribe;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";

    private TabAdapter tabAdapter;
    private TextView tvSongTime;
    private SeekBar seekBar;
    private ImageButton btnPlayAndStop;


    private int currentTab = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setVolumeControlStream(AudioManager.STREAM_MUSIC); //for volume
        setContentView(R.layout.activity_main);
        SongManager.initialize();
        setUI();
        startService(new Intent(MainActivity.this, PlayerService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        SongManager.getInstance().getBus().register(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        SongManager.getInstance().getBus().unregister(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_settings:
//                //TODO implement on menu item click action
//                toast("settings coming soon");
//                return true;
//            default:
        return super.onOptionsItemSelected(item);
//        }
    }


    /**
     * This method initializes user interface
     */
    private void setUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitleTextColor(ContextCompat
                    .getColor(getApplicationContext(), R.color.icons));
            setSupportActionBar(toolbar);
        }
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabAdapter = new TabAdapter(getSupportFragmentManager(), 2);
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(pageChanger);

        currentTab = viewPager.getCurrentItem();
        viewPager.setCurrentItem(currentTab);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager); //ставим вкладки


        ImageButton btnNext = (ImageButton) findViewById(R.id.buttonNext);
        ImageButton btnPrev = (ImageButton) findViewById(R.id.buttonPrev);
        btnPlayAndStop = (ImageButton) findViewById(R.id.buttonPlayStop);
        tvSongTime = (TextView) findViewById(R.id.textViewSongTime);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        btnPlayAndStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Song song = SongManager.getInstance().getSong(getApplicationContext(),
                            SongManager.currentSongPosition, getCurFrag().getPlaylist());
                    PlayerService.getInstance().play(song);
                } catch (SongListException e) {
                    Log.e(TAG, "onClick: Song not found", e);
                }
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextSong();
            }
        });
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevSong();
            }
        });
    }


    /**
     * This method return current fragment.
     *
     * @return PlaylistFragment
     */
    private PlaylistFragment getCurFrag() {
        return (PlaylistFragment) tabAdapter.getItem(currentTab);
    }


    /**
     * This method switch next song
     */
    public void nextSong() {
        try {
            int s = SongManager.getInstance().getPlaylist(this, getCurFrag()
                    .getPlaylist()).size() - 1;
            if (SongManager.currentSongPosition == s) {
                SongManager.currentSongPosition = 0;
            } else {
                SongManager.currentSongPosition++;
            }
            Song song = SongManager.getInstance()
                    .getSong(this, SongManager.currentSongPosition, getCurFrag().getPlaylist());
            PlayerService.getInstance().playNewSong(song);
            SongManager.getInstance().getBus()
                    .post(new ScrollRecyclerViewEvent(SongManager.currentSongPosition));
        } catch (SongListException e) {
            Log.e(TAG, "nextSong: Error. Song not found.", e);
        }
    }


    /**
     * This method switch previous song
     */
    public void prevSong() {

        try {
            if (SongManager.currentSongPosition == 0) {
                SongManager.currentSongPosition = SongManager.getInstance()
                        .getPlaylist(this, getCurFrag().getPlaylist()).size() - 1;
            } else {
                SongManager.currentSongPosition--;
            }
            Song song = SongManager.getInstance()
                    .getSong(this, SongManager.currentSongPosition, getCurFrag().getPlaylist());
            PlayerService.getInstance().playNewSong(song);
            SongManager.getInstance().getBus()
                    .post(new ScrollRecyclerViewEvent(SongManager.currentSongPosition));
        } catch (SongListException e) {
            Log.e(TAG, "prevSong: Error. Song not found.", e);
        }
    }


    private void seekChange(View v) {
        SeekBar sb = (SeekBar) v;
        PlayerService.getInstance().mediaPlayerSeekTo(sb.getProgress());
    }


    private ViewPager.OnPageChangeListener pageChanger = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            currentTab = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };


    @Subscribe
    public void on(PlayPauseEvent event) {
        boolean isPlaying = event.isPlaying();
        if (!isPlaying) {
            btnPlayAndStop.setImageResource(R.drawable.ic_play_arrow_white_48dp);
        } else {
            btnPlayAndStop.setImageResource(R.drawable.ic_pause_white_48dp);
        }
    }


    @Subscribe
    public void on(SeekbarUpdateEvent event) {
        seekBar.setProgress(event.getPosition());
        if (seekBar.getProgress() >= PlayerService.getInstance().getDuration() - 1000) {
            nextSong();
        }
    }


    @Subscribe
    public void on(TimeDurationEvent event) {
        String time = event.getTime();
        tvSongTime.setText(time);
    }


    @Subscribe
    public void on(SetSeekBarEvent event) {
        seekBar.setMax(event.getDuration());
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                seekChange(v);
                return false;
            }
        });
    }
}
