package com.intemdox.intemdoxplayer.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intemdox.intemdoxplayer.R;
import com.intemdox.intemdoxplayer.SongManager;
import com.intemdox.intemdoxplayer.adapters.PlaylistAdapter;
import com.intemdox.intemdoxplayer.events.InsertItemViewEvent;
import com.intemdox.intemdoxplayer.events.RemoveItemViewEvent;
import com.intemdox.intemdoxplayer.events.ScrollRecyclerViewEvent;
import com.intemdox.intemdoxplayer.events.UpdateItemViewEvent;
import com.squareup.otto.Subscribe;


public class PlaylistFragment extends Fragment {

//    public static final String TAG = "PlaylistFragment";
    public static final int FULL_LIST = 0;
    public static final int LIKED_LIST = 1;
    public static final String LIST_TYPE = "list_current_type";

    private RecyclerView mainList;
    private PlaylistAdapter playlistAdapter;

    private int type = 0;
    private int playlist;


    public PlaylistFragment() {
    }


    /**
     * This method created a new instance of PlaylistFragment or just return existing
     *
     * @param key   - key.
     * @param value - value.
     * @return instance of PlaylistFragment.
     */
    public static PlaylistFragment newInstance(String key, int value) {
        PlaylistFragment myFragment = new PlaylistFragment();
        Bundle args = new Bundle();
        args.putInt(key, value);
        myFragment.setArguments(args);
        return myFragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        SongManager.getInstance().getBus().register(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        SongManager.getInstance().getBus().unregister(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey(LIST_TYPE)) {
            type = args.getInt(LIST_TYPE);
            switch (type) {
                case 0:
                    playlist = 0;
                    break;
                case 1:
                    playlist = 1;
                    break;
                default:
                    playlist = 0;
                    break;
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_player, container, false);
        mainList = (RecyclerView) root.findViewById(R.id.recycler_current);
        mainList.setLayoutManager(new LinearLayoutManager(getContext()));
        initializeList();
        return root;
    }


    /**
     * This method initializes list of song for each fragment.
     */
    private void initializeList() {
        switch (type) {
            case FULL_LIST:
                initializeFullList();
                break;
            case LIKED_LIST:
                initializeLikedList();
                break;
        }
    }


    /**
     * This method initializes main playlist.
     */
    private void initializeFullList() {
        SongManager.getInstance().initializeBasicPlaylist(this.getContext());
        setupAdapter(FULL_LIST);
    }


    /**
     * This method initialized list of lover songs.
     */
    private void initializeLikedList() {
        SongManager.getInstance().initializeLikedlist();
        setupAdapter(LIKED_LIST);
    }


    /**
     * This list setups adapter for RecyclerView.
     *
     * @param playlists - playlist, wor which setups adapter.
     */
    private void setupAdapter(int playlists) {
        playlistAdapter = new PlaylistAdapter(this.getContext(), playlists);
        mainList.setAdapter(playlistAdapter);
    }


    public int getPlaylist() {
        return playlist;
    }


    @Subscribe
    public void on(UpdateItemViewEvent event) {
        event.getClass();
        playlistAdapter.notifyDataSetChanged();
    }


    @Subscribe
    public void on(InsertItemViewEvent event) {
        event.getClass();
        playlistAdapter.notifyItemInserted(event.getPosition());
    }


    @Subscribe
    public void on(RemoveItemViewEvent event) {
        event.getClass();
        playlistAdapter.notifyItemRemoved(event.getPosition());
    }


    @Subscribe
    public void on(ScrollRecyclerViewEvent event) {
        event.getClass();
        mainList.scrollToPosition(event.getIndex());
    }


}
